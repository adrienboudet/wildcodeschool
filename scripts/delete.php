<?php
require '../constants.php';
require 'connexion.php';
$id = $_GET['id'];

$deleteNameQuery = $db->prepare('DELETE FROM `members` WHERE member_id = :id');
$deleteNameQuery->execute(['id' => $id]) or die(print_r($db->errorInfo()));
header('Location: /WILDCODESCHOOL');