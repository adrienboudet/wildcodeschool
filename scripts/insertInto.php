<?php
require '../constants.php';
require 'connexion.php';

//conditions to trigger the INSERT INTO query
if (isset($_POST['name']) && !empty($_POST['name'])) {
  // Query to add a name in database
  $insertQuery = 'INSERT INTO members(name) VALUES (:name)';
  $insertName = $db->prepare($insertQuery);
  $insertName->execute(['name' => $_POST['name']]);
}
header('Location: /WILDCODESCHOOL');
